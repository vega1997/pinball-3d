﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class score_script : MonoBehaviour {


	private float score = 0;
	public TextMesh score_text;
	private int num_target=0;
	public GameObject target1;
	public GameObject target2;
	public GameObject target3;
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		score_text.text = "" + score;
	}

	public void updateScore(float newScore){
		score += newScore;
	}
	public void updateTarget(){
		num_target++;
		if (num_target == 3) {
			score = score*3;
			Invoke("setTargets", 2);

		}
	}
	void setTargets(){
		target1.gameObject.active = true;
		target2.gameObject.active = true;
		target3.gameObject.active = true;
		num_target = 0;
	}
}