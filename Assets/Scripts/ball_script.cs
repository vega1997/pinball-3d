﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball_script : MonoBehaviour {

	public int num_lifes = 3;
	public TextMesh lifes;
	public TextMesh game_over_text;
	Rigidbody rbody;
	float originz;
	float originy;
	float originx;

	void Start () {
		originz = this.transform.localPosition.z;
		originy = this.transform.localPosition.y;
		originx = this.transform.localPosition.x;
		rbody = this.gameObject.GetComponent<Rigidbody>();
	}

	void OnCollisionEnter(Collision coll){

		if (coll.collider.gameObject.layer == LayerMask.NameToLayer ("end")) {
			if (num_lifes > 0) {
				num_lifes--;
				this.transform.localPosition = new Vector3 (originx, originy, originz);
				lifes.GetComponent<lifes_script> ().updateLifes(1);
			} else if (num_lifes == 0) {
				Destroy(gameObject);
				game_over_text.GetComponent<game_over_script>().updateText("GAME OVER");
			}
		} else if (coll.collider.gameObject.layer == LayerMask.NameToLayer ("bumper")) {
			rbody.AddForce(transform.forward * 500);
		}
	}

}
