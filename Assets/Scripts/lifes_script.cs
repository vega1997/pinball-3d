﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifes_script : MonoBehaviour {

	public TextMesh lifes_text = null;
	private int lifes = 3;
	void Start(){
	}

	void Update()
	{
		lifes_text.text = "" + lifes;
	}

	public void updateLifes(int newLifes)
	{
		lifes -= newLifes;
	}
}