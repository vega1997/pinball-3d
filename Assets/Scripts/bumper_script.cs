﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumper_script : MonoBehaviour {

	public TextMesh score;
	private Light light;
	void Start () {
		//score = GameObject.Find("num_text");
		light = this.GetComponent<Light>();

	}
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter(Collision coll){
		if (coll.collider.gameObject.layer == LayerMask.NameToLayer ("ball")) {
			score.GetComponent<score_script> ().updateScore(1);
			light.intensity = 20;
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play();
			Invoke("setLight", 0.10f);
		}
	}
	void setLight(){
		light.intensity = 0;
	}
	

}
