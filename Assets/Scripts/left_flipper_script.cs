﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class left_flipper_script : MonoBehaviour {

	Rigidbody rbody;
	private float force = 3000;
	private Vector3 forceDirection = Vector3.forward;

	// Update is called once per frame
	void Start()
	{
		rbody = this.gameObject.GetComponent<Rigidbody>();
	}

	void Update ()
	{
		if( Input.GetKeyDown( KeyCode.Z ))
		{
			rbody.AddForceAtPosition(forceDirection.normalized * force, transform.position);
		}

		if( Input.GetKeyUp( KeyCode.Z ))
		{
			rbody.AddForceAtPosition(forceDirection.normalized * -force, transform.position);
		}


	}
};