﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llanca_script : MonoBehaviour {

	Rigidbody rbody;
	float forcedown;
	float forceup;
	void Start () {
		rbody = this.gameObject.GetComponent<Rigidbody>();
		forcedown = 500f;
		forceup = 1000f;
	}

	void Update() {
		if (Input.GetKey(KeyCode.Space)){
			rbody.AddForce(-transform.forward * forcedown);
		}
		else{
			rbody.AddForce(transform.forward * forceup);
		}
	}
}

