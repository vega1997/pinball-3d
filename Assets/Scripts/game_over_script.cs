﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class game_over_script : MonoBehaviour {


	public TextMesh game_over_text;
	private string game_over;
	void Start () {
		game_over ="SCORE";
	}

	// Update is called once per frame
	void Update () {
		game_over_text.text = game_over;
	}

	public void updateText(string newText){
		game_over = newText;
	}
}
